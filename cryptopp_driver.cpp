#include "cryptopp_driver.h"

#include <cryptopp/base64.h>
#include <cryptopp/filters.h>

std::string runBenchmark (std::string &input, CryptoPP::HashTransformation &algo) {
	std::string output;
	
	CryptoPP::StringSource hasher(input, true,
		new CryptoPP::HashFilter(algo,
			new CryptoPP::Base64Encoder (
				new CryptoPP::StringSink(output)
			)
		)
	);

	return output;
}
