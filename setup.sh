#!/usr/bin/env bash

# update repository
#git pull -u origin master

# recompile the benchmarks
#g++ -Wall -o benchmark benchmark.cpp cryptopp_driver.cpp -lpapi -static -lcryptopp
g++ -Wall -o benchmark benchmark.cpp -lpapi -static -lcryptopp

# Done.
echo 'If no errors were reported, run the benchmark with the following commands:'
echo '   cd cs5222'
echo '   ./benchmark'
