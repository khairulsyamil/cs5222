#ifndef __CRYPTOPP_DRIVER_H__
#define __CRYPTOPP_DRIVER_H__

#include <string.h>
#include <cryptopp/cryptlib.h>

std::string runBenchmark (std::string &input, CryptoPP::HashTransformation &algo);

#endif
