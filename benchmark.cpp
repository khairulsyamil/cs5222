#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <iterator>

#include <sys/types.h>
#include <unistd.h>
#include <sched.h>
#include <errno.h>

#include <cstdio>

#include <papi.h>

#include <cryptopp/cryptlib.h>
#include <cryptopp/base64.h>
#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/sha3.h>
#include <cryptopp/whrlpool.h>
#include <cryptopp/tiger.h>

#define MAX_RUNS 10
#define OUTPUT_FILE "output.txt"

typedef long long hw_cnt_t;
typedef struct _report_t {
	std::string algo;
	std::string input;
	int run;
	int event;
	long long value;
} report_t;

void setupEventsToCapture (std::list<int> &eventsToRecord);
void setupAlgorithms (std::list<CryptoPP::HashTransformation *> &algorithms);
void setupInputFiles (std::list<std::string> &inputFiles);
int setupEventSet (std::list<int> &eventsList, std::list<int> &currentEvents, unsigned int maxToAdd);

void addToReport (std::list<report_t> &reportList, report_t &reportStruct, std::list<int> &eventSet, hw_cnt_t *values, int valuesSize);
void writeReport (std::list<report_t> &reportList);

int main (int argc, char** argv) {
	std::list<int> eventsToCapture;
	std::list<int> currentCaptureSet;
	std::list<CryptoPP::HashTransformation *> algorithms;
	std::list<std::string> inputFiles;

	pid_t pid = getpid();
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(0, &mask);
	if (sched_setaffinity(pid, sizeof(mask), &mask) == -1) {
		std::cout << "Warning: Could not set CPU affinity: " << strerror(errno) << std::endl;
	}

	// Setup PAPI.
	PAPI_library_init(PAPI_VER_CURRENT);
	int maxCounts = PAPI_num_counters();
	std::cout << "Max counters: " << maxCounts << std::endl;

	hw_cnt_t *values = new hw_cnt_t[maxCounts];
	report_t reportStruct;

	setupAlgorithms(algorithms);
	setupInputFiles(inputFiles);

	// Report format.
	std::list<report_t> reportList;

	for (std::list<CryptoPP::HashTransformation *>::iterator algoIter = algorithms.begin();
		 algoIter != algorithms.end();
		 ++algoIter) {

		reportStruct.algo = std::string ((*algoIter)->AlgorithmName());
		std::cout << "Running for " << reportStruct.algo << std::endl;

		for (std::list<std::string>::iterator inputIter = inputFiles.begin();
			 inputIter != inputFiles.end();
			 ++inputIter) {

			reportStruct.input = *inputIter;

			// Setup the event set.
			setupEventsToCapture(eventsToCapture);

			int eventSetIdCount = 0;
			while (!eventsToCapture.empty()) {
				std::list<int> currentSet;
				int eventSet = setupEventSet(eventsToCapture, currentSet, maxCounts);

				for (int run = 1; run <= MAX_RUNS; run++) {
					std::ifstream inputFile((*inputIter).c_str());
					std::string inputData((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
					std::string output;
					CryptoPP::HashFilter *filter = new CryptoPP::HashFilter(
						*(*algoIter),
						new CryptoPP::Base64Encoder (
							new CryptoPP::StringSink(output)
						)
					); 

					reportStruct.run = run;

					std::cout << "Running for: " << reportStruct.algo
							  << ", input: " << reportStruct.input
							  << ", eventSetId: " << eventSetIdCount
							  << ", run: " << reportStruct.run << std::endl;

					PAPI_start(eventSet);
					CryptoPP::StringSource hasher(inputData, true, filter);
					PAPI_stop(eventSet, values);

					addToReport(reportList, reportStruct, currentSet, values, maxCounts);
					PAPI_reset(eventSet);
				}

				PAPI_cleanup_eventset(eventSet);
				PAPI_destroy_eventset(&eventSet);

				eventSetIdCount++;
			}
		}
	}

	std::cout << "Writing report to " << OUTPUT_FILE << std::endl;
	writeReport(reportList);
	std::cout << "Report written" << std::endl;

	PAPI_shutdown();
	delete[] values;

	return 0;
}

void setupEventsToCapture (std::list<int> &eventsToRecord) {
	// Set up parameters that we want to capture.
	eventsToRecord.push_back(PAPI_L1_DCM);
	eventsToRecord.push_back(PAPI_L1_ICM);
	eventsToRecord.push_back(PAPI_L2_DCM);
	eventsToRecord.push_back(PAPI_L2_ICM);
	eventsToRecord.push_back(PAPI_L3_DCM);
	eventsToRecord.push_back(PAPI_L3_ICM);
	eventsToRecord.push_back(PAPI_L1_TCM);
	eventsToRecord.push_back(PAPI_L2_TCM);
	eventsToRecord.push_back(PAPI_L3_TCM);
	eventsToRecord.push_back(PAPI_L1_LDM);
	eventsToRecord.push_back(PAPI_L1_STM);
	eventsToRecord.push_back(PAPI_L2_LDM);
	eventsToRecord.push_back(PAPI_L2_STM);
	eventsToRecord.push_back(PAPI_L3_LDM);
	eventsToRecord.push_back(PAPI_L3_STM);
	eventsToRecord.push_back(PAPI_L3_DCH);
	eventsToRecord.push_back(PAPI_REF_CYC);
	eventsToRecord.push_back(PAPI_TOT_CYC);
	eventsToRecord.push_back(PAPI_TOT_INS);
	eventsToRecord.push_back(PAPI_L1_DCH);
	eventsToRecord.push_back(PAPI_L2_DCH);
	eventsToRecord.push_back(PAPI_L1_DCA);
	eventsToRecord.push_back(PAPI_L2_DCA);
	eventsToRecord.push_back(PAPI_L3_DCA);
	eventsToRecord.push_back(PAPI_L1_DCR);
	eventsToRecord.push_back(PAPI_L2_DCR);
	eventsToRecord.push_back(PAPI_L3_DCR);
	eventsToRecord.push_back(PAPI_L1_DCW);
	eventsToRecord.push_back(PAPI_L2_DCW);
	eventsToRecord.push_back(PAPI_L3_DCW);
	eventsToRecord.push_back(PAPI_L1_ICH);
	eventsToRecord.push_back(PAPI_L2_ICH);
	eventsToRecord.push_back(PAPI_L3_ICH);
	eventsToRecord.push_back(PAPI_L1_ICA);
	eventsToRecord.push_back(PAPI_L2_ICA);
	eventsToRecord.push_back(PAPI_L3_ICA);
	eventsToRecord.push_back(PAPI_L1_ICR);
	eventsToRecord.push_back(PAPI_L2_ICR);
	eventsToRecord.push_back(PAPI_L3_ICR);
	eventsToRecord.push_back(PAPI_L1_ICW);
	eventsToRecord.push_back(PAPI_L2_ICW);
	eventsToRecord.push_back(PAPI_L3_ICW);
	eventsToRecord.push_back(PAPI_L1_TCH);
	eventsToRecord.push_back(PAPI_L2_TCH);
	eventsToRecord.push_back(PAPI_L3_TCH);
	eventsToRecord.push_back(PAPI_L1_TCA);
	eventsToRecord.push_back(PAPI_L2_TCA);
	eventsToRecord.push_back(PAPI_L3_TCA);
	eventsToRecord.push_back(PAPI_L1_TCR);
	eventsToRecord.push_back(PAPI_L2_TCR);
	eventsToRecord.push_back(PAPI_L3_TCR);
	eventsToRecord.push_back(PAPI_L1_TCW);
	eventsToRecord.push_back(PAPI_L2_TCW);
	eventsToRecord.push_back(PAPI_L3_TCW);
}

void setupAlgorithms (std::list<CryptoPP::HashTransformation *> &algorithms) {
	algorithms.push_back(new CryptoPP::SHA1());
	algorithms.push_back(new CryptoPP::SHA256());
	algorithms.push_back(new CryptoPP::SHA512());
	algorithms.push_back(new CryptoPP::SHA3_256());
	algorithms.push_back(new CryptoPP::SHA3_512());
	algorithms.push_back(new CryptoPP::Tiger());
	algorithms.push_back(new CryptoPP::Whirlpool());
}

void setupInputFiles (std::list<std::string> &inputFiles) {
	inputFiles.push_back("input/l1.txt");
	inputFiles.push_back("input/l2.txt");
	inputFiles.push_back("input/l3.txt");
	inputFiles.push_back("input/l4.txt");
}

int setupEventSet (std::list<int> &eventsList, std::list<int> &currentEvents, unsigned int maxToAdd) {
	int eventSetId = PAPI_NULL;
	char eventName[1024] = {0};

	PAPI_create_eventset(&eventSetId);

	currentEvents.clear();

	for (std::list<int>::iterator eventIter = eventsList.begin();
		 eventIter != eventsList.end() && currentEvents.size() <= maxToAdd;
		 ++eventIter) {
		int event = *eventIter;

		int retVal = PAPI_add_event(eventSetId, event);
		PAPI_event_code_to_name(event, &eventName[0]);
		if (retVal >= 0) {
			currentEvents.push_back(event);
			//std::cout << "Added " << eventName << std::endl;
			eventIter = eventsList.erase(eventIter);
		}
		else if (retVal != -1) {
			//std::cout << "Ignoring " << eventName << "(" << retVal << ")" << std::endl;
			eventIter = eventsList.erase(eventIter);
		}
	}

	return eventSetId;
}

void addToReport (std::list<report_t> &reportList, report_t &reportStruct, std::list<int> &eventSet, hw_cnt_t *values, int valuesSize) {
	int i = 0;
	for (std::list<int>::iterator eventIter = eventSet.begin();
		 eventIter != eventSet.end();
		 ++eventIter, ++i) {

		reportStruct.event = *eventIter;
		reportStruct.value = values[i];

		reportList.push_back(reportStruct);
	}
}

void writeReport (std::list<report_t> &reportList) {
	FILE *output = fopen(OUTPUT_FILE, "w");
	char eventName[1024] = {0};

	for (std::list<report_t>::iterator reportIter = reportList.begin();
		 reportIter != reportList.end();
		 ++reportIter) {
		int eventCode = (*reportIter).event;
		std::string algo  = (*reportIter).algo;
		std::string input = (*reportIter).input;
		PAPI_event_code_to_name(eventCode, &eventName[0]);
		fprintf(output, "\"%s\",\"%s\",%d,%s,%lld\n", algo.c_str(), input.c_str(), (*reportIter).run, eventName, (*reportIter).value);

	}

	fclose(output);
}

